
function showCities () {
   var country = document.getElementById("countryName").value;

   // Replace the two lines below with your implementation
   //var mock_text = "You should show a list of clickable cities of " + country;
  // document.getElementById("left").innerHTML = mock_text;
   var req = new XMLHttpRequest();
   var uri = "city_names.php?countryName="+country;
    req.open('GET', uri, /*async*/true);
    req.onreadystatechange = function() {
      if (req.readyState == 4 && req.status == 200) {
        var city_list = JSON.parse(req.responseText);
        var html_list = "";
        for (var i = 0; i < city_list.length; i++) {
            html_list +=  "<p><a href=\"javascript:showWeather('"+city_list[i]+"');\">"+city_list[i]+"</a></p>";
        };
        document.getElementById("left").innerHTML = html_list;
        document.getElementById("right").innerHTML = "<p>&#8612; Select a city from the left menu</p>";
      }
    };
    req.send(/*no params*/null);
    document.getElementById("left").innerHTML ="Getting city names ...";
};


function showWeather (city) {
    var country = document.getElementById("countryName").value;
    var uri = "city_info.php?CityName="+encodeURI(city)+"&CountryName="+encodeURI(country);
    var req = new XMLHttpRequest();
    req.open('GET', uri, /*async*/true);
    req.onreadystatechange = function() {
      if (req.readyState == 4 && req.status == 200) {
        var info = JSON.parse(req.responseText);
        var show = "<table>";
        for (var prop in info) {
            show += "<tr><td>"+prop+": </td><td>"+info[prop]+"</td></tr>";
        };
        show += "</table>";
        document.getElementById("right").innerHTML= show;
      }
    };
    req.send(/*no params*/null);
    document.getElementById("right").innerHTML="Processing request ...";
}

window.onload = showCities();
