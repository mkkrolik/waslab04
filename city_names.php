<?php
ini_set("soap.wsdl_cache_enabled","0");

try{

  $sClient = new SoapClient('http://www.webservicex.net/globalweather.asmx?WSDL');
  
  $respondObject = new stdClass();
  $respondObject->CountryName = $_GET['countryName'];
  
  $result = $sClient->GetCitiesByCountry($respondObject);

  $results = new SimpleXMLElement($result->GetCitiesByCountryResult);
  $cities = array();
  
  foreach($results as $result) {  
       array_push($cities, (string)$result->City);
  }    
   
  $array = [settype($cities[0], "string")];
  
  echo json_encode($cities);

  #echo '["Not","Yet","Implemented"]';
  
}
catch(SoapFault $e){	
  header(':', true, 500);
  echo json_encode($e);
}

function xmlpp($xml) {  
    $xml_obj = new SimpleXMLElement($xml);  
    $level = 4;  
    $indent = 0; // current indentation level  
    $pretty = array();  
      
    // get an array containing each XML element  
    $xml = explode("\n", preg_replace('/>\s*</', ">\n<", $xml_obj->asXML()));  
  
    // shift off opening XML tag if present  
    if (count($xml) && preg_match('/^<\?\s*xml/', $xml[0])) {  
      $pretty[] = array_shift($xml);  
    }  
  
    foreach ($xml as $el) {  
      if (preg_match('/^<([\w])+[^>\/]*>$/U', $el)) {  
          // opening tag, increase indent  
          $pretty[] = str_repeat(' ', $indent) . $el;  
          $indent += $level;  
      } else {  
        if (preg_match('/^<\/.+>$/', $el)) {              
          $indent -= $level;  // closing tag, decrease indent  
        }  
        if ($indent < 0) {  
          $indent += $level;  
        }  
        $pretty[] = str_repeat(' ', $indent) . $el;  
      }  
    }     
    $xml = implode("\n", $pretty);     
    return $xml;  
}  

